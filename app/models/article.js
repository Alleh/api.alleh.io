'use strict'

var mongoose = require('mongoose');
var Schema  = mongoose.Schema;

var ArticleSchema   = new Schema({
	title: String,
	body: String,
	markdown: String,
	author: String,
	slug: String,
	published: Boolean,
	tags: [{ name: String }],
 	created_at: { type: Date, default: Date.now },

});

module.exports = mongoose.model('Article', ArticleSchema);
