var articleModel = require('../models/article.js');
var mongoose = require('mongoose');
var db = {};
// TODO: fixa asap, messy.. just for testing some stuff..
db.connect = function() {
    mongoose.connect('mongodb://localhost/test');
}

db.populateDb = function() {
    var a = new articleModel();
    a.title = 'random title';
    a.body = 'random body for tests';
    a.published = Date.now();
    a.tags = [{name: 'tag1'},{name: 'tag2'}];
    a.save();

    var b = new articleModel();
    b.title = 'OH MUST READ THIS.';
    b.body = 'just kidding, it sucks..';
    b.published = Date.now();
    b.tags = [{name: 'random'},{name: 'jk'}, {name: 'tag1'}];
    b.save();

    var c = new articleModel();
    c.title = 'randoms title22';
    c.body = 'what is this? just some random text.';
    c.published = Date.now();
    c.tags = [{name: 'hehe'},{name: 'tag2'}];
    c.save();

    var d = new articleModel();
    d.title = 'Aye';
    d.body = 'booooooo..';
    d.published = Date.now();
    d.tags = [{name: 'tag1'},{name: 'tag2'}];
    d.save();

    var e = new articleModel();
    e.title = 'my post is the most important one';
    e.body = 'just because im the one and only.';
    e.published = Date.now();
    e.tags = [{name: 'theone'},{name: 'random'}];
    e.save();


    var f = new articleModel();
    f.title = 'nonsense here just a title';
    f.body = 'this is totally worth it, im bored.';
    f.published = Date.now();
    f.tags = [{name: 'tag1'},{name: 'jk'}];
    f.save();

    var g = new articleModel();
    g.title = 'title';
    g.body = 'tests data for tests';
    g.published = Date.now();
    g.tags = [{name: 'test'},{name: 'tag2'}];
    g.save();

}

db.clear = function(){
    articleModel.remove({

    }, function(err, article) {
        if (err)
            console.log(err);
    });
}
module.exports = db;
