'use strict'
var express   = require('express'),
    articles  = require('./controllers/articles.js'),
    router    = express.Router();

router.get('/test', articles.list);
router.get('/articles', articles.list);
router.get('/articles/:id', articles.show);
router.post('/articles', articles.add);
router.put('/articles/:id', articles.edit);
router.delete('/articles/:id', articles.destroy);

module.exports = router;
