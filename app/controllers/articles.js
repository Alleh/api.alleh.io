'use strict'

var article = require('../models/article'),
    _ = require('lodash');


var articles = {

    list: function(req,res){
        article.find(function(err, data){
            if (err) {
                return res.status(404).json({ message: err});
            }

            return res.status(200).json(data);
        });
    },

    show: function(req,res) {

        article.findOne({'title': req.params.id.split('-').join(' ')}, function(err, data){
            if (err || !data) {
                return res.status(404).json({message:'Could not find anything.'});
            }
            return res.status(200).json(data);
        });
    },

    add: function(req, res){

        if (!req.body.title || !req.body.body) {
            return res.status(400).json({message: 'body and title need to be specified'});
        }

        var post = new article({
            "title": req.body.title,
            "body": req.body.body,
            "published": req.body.true
        });
        var tags = req.body.tags.split(',');
        for(var i in tags){
            post.tags.push({name: tags[i]});
        }
        post.save(function(err) {
            if (err)
                 return res.status(500).send(err);
             return res.status(201).send({ message: 'article created!' });
        });
    },

    edit: function(req, res){
        article.findById({'_id': req.params.id }, function(err, article){
          if (err) return res.status(404).json({message: err});
            article.title = req.body.title;
            article.body = req.body.body;
            article.published = req.body.published;
            var tags = req.body.tags.split(',');
            for(var i in tags){
                post.tags.push({name: tags[i]});
            }
        article.save(function(err) {
          if (err) return res.status(500).send(err);

            return res.status(204).send({ message: 'article updated!' });
        });
      });
    },

    destroy: function(req, res){
        article.remove({
          _id: req.params.id
    	}, function(err, article) {
        if (err) return res.status(500).send(err);

          return res.status(200).send({ message: 'article deleted!' });
        });
    }
}


module.exports = articles;
