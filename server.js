var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var router = require('./app/router.js');
var db = require('./app/configs/db.js');
var port = process.env.PORT || 5959;

db.connect();
db.populateDb();

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);
app.listen(port);

console.log('Starting app at port: ' + port);

module.exports = app;
