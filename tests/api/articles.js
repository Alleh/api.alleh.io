'use strict'

var assert = require('assert');
var should = require('should');
var expect = require('chai').expect
var app = require('../../server.js');
var request = require('supertest');
var db = require('../../app/configs/db.js');

describe('API: Articles', function() {
    beforeEach(function(done) {
        db.populateDb();
        done();
    });
    afterEach(function(done) {
        db.clear();
        done();
    });

    describe('List', function(done) {

        it('should respond with statuscode 200', function(done){
            request(app)
                .get('/api/articles')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

        it('should get all objects.', function(done){
            var data = request(app)
                .get('/api/articles')
                .set('Accept', 'application/json')
                .expect(200)
                .end(function(err, data){
                    if (err) return done(err);
                    expect(data.body).to.have.length(7);
                    done();
                });
        });
    });
    describe('Show', function(done) {

        it('should respond with statuscode 200', function(done){
            request(app)
                .get('/api/articles/Aye')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200,done)
        });

        it('should return an object', function(done){
            request(app)
                .get('/api/articles/Aye')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, data){
                    if (err) return done(err);
                    expect(data.body).to.be.a('object');
                    should.exist(data.body.title);
                    should.exist(data.body.body);
                    should.exist(data.body.tags);
                    expect(data.body.title).to.equal('Aye');
                    done();
                });
        });

        it('should respond with statuscode 404 if nothing is found', function(done){
            request(app)
                .get('/api/articles/thisdoesnotexists')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(404,done)
        });
    });

    describe('Add', function(done) {
        var requestObject = {
            'title': 'new title here',
            'body': 'body here',
            'tags': 'cool,meh,derp,herp,hehehe lols, haha, baaah',
            'published': true
        }

        it('should respond with statuscode 201', function(done){
            request(app)
                .post('/api/articles')
                .set('Accept', 'application/json')
                .send(requestObject)
                .expect('Content-Type', /json/)
                .expect(201, done);
        });

        it('should respond with statuscode 400 if title is missing', function(done){
            requestObject.title = null;
            request(app)
                .post('/api/articles')
                .set('Accept', 'application/json')
                .send(requestObject)
                .expect('Content-Type', /json/)
                .expect(400, done);
        });
        it('should respond with statuscode 400 if body is missing', function(done){
            requestObject.body = null;
            request(app)
                .post('/api/articles')
                .set('Accept', 'application/json')
                .send(requestObject)
                .expect('Content-Type', /json/)
                .expect(400, done);
        });
    });

	xdescribe('Edit', function(done) {
		var requestObject = {
			'title': 'new title here mkay',
			'body': 'body here lol edited',
			'tags': 'cool,meh,derp,herp,hehehe lols, haha, baaah',
			'published': true
		}

		it('should respond with statuscode 204 if everything is OK', function(done){
			request(app)
				.put('/api/articles')
				.set('Accept', 'application/json')
				.send(requestObject)
				.expect('Content-Type', /json/)
				.expect(204, done);
		});

		it('should respond with statuscode 400 if title is missing', function(done){
			requestObject.title = null;
			request(app)
				.put('/api/articles')
				.set('Accept', 'application/json')
				.send(requestObject)
				.expect('Content-Type', /json/)
				.expect(400, done);
		});
		it('should respond with statuscode 400 if body is missing', function(done){
			requestObject.body = null;
			request(app)
				.put('/api/articles')
				.set('Accept', 'application/json')
				.send(requestObject)
				.expect('Content-Type', /json/)
				.expect(400, done);
		});
	});

	xdescribe('Destroy', function(done) {

		it('should respond with statuscode 200 if everything is OK', function(done){
			request(app)
				.delete('/api/articles/random-title')
				.set('Accept', 'application/json')
				.expect('Content-Type', /json/)
				.expect(200, done);
		});
	});
});
